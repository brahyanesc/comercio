from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from rest_framework.documentation import include_docs_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('proyecto_core.routers')),
    path('api/', include('apps.users.urls')),
    #path('docs/', include_docs_urls(title='Comercio API'), name='api-docs'),
    
]
