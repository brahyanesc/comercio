from rest_framework.routers import DefaultRouter
from apps.deliveries import views as deliveriesViews
from apps.payments import views as paymentsViews
from apps.shops import views as shopsViews
from apps.users import views as usersViews
from django.urls.conf import include
from django.urls import path

router = DefaultRouter()

router.register('user',usersViews.UserViewSet)
router.register('delivery',deliveriesViews.DeliveryViewSet)
router.register('payment',paymentsViews.PaymentViewSet)
router.register('product',shopsViews.ProductViewSet)
router.register('order',shopsViews.OrderViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
