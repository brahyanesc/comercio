from proyecto_core.celery import  app
from django.conf import settings


from django.core.mail import send_mail

@app.task
def send_mail_async(subject, msg, to):
    """Send email notificación"""
    send_mail(
        subject,
        msg,
        settings.EMAIL_HOST_USER,
        to,
        fail_silently=False,
    )
    return True