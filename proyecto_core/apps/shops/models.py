from django.db.models.enums import Choices
from apps.users.models import User
from django.db import models
from apps.deliveries.models import Delivery
from apps.payments.models import Payment
from django.conf import settings


class Product(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(max_length=1000)
    value = models.DecimalField(decimal_places=2, max_digits=12)

    class Meta:
        verbose_name='Product'
        verbose_name_plural='Products'
    
    def __str__(self):
        return self.name

class Order(models.Model):
    """Purchase Order"""

    class Status(models.TextChoices):
        PROCCESING = 'PR', 'processing'
        PAID_OUT = 'PO', 'paid out'
        SENT = 'SE', 'sent'

    date_order = models.DateTimeField(auto_now_add=True)
    status = models.CharField(choices=Status.choices, max_length=255)
    products = models.ManyToManyField(Product)
    deliveries = models.ManyToManyField(Delivery, related_name='orders')
    payments = models.ManyToManyField(Payment, blank= True, related_name='orders')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name='Order'
        verbose_name_plural='Orders'
    
    def __str__(self):
        return 'order #: {} date: {}'.format(self.pk, self.date_order.__str__()) 

