from rest_framework import permissions, viewsets, authentication
from rest_framework.authentication import TokenAuthentication
from .models import Order, Product
from .serializers import OrderSerializer, ProductSerializer



class OrderViewSet(viewsets.ModelViewSet):
    """CRUD Order"""
    authentication_classes = (TokenAuthentication,)
    serializer_class = OrderSerializer
    queryset = Order.objects.all()
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes =(permissions.IsAuthenticated,)

class ProductViewSet(viewsets.ModelViewSet):
    """CRUD product"""
    authentication_classes = (TokenAuthentication,)
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes =(permissions.IsAuthenticated,)




