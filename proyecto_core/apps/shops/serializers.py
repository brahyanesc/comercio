from rest_framework import serializers
from .models import Order, Product
from apps.users.serializers import UserSerializer

class OrderSerializer(serializers.ModelSerializer):
    """Order serializar"""
    class Meta:
        model = Order
        fields = ('id','date_order', 'status', 'user','products', 'deliveries', 'payments',)


class ProductSerializer(serializers.ModelSerializer):
    """Product serializer"""
    class Meta:
        model = Product
        fields = ('id','name', 'description', 'value',)
