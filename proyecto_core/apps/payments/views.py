from rest_framework import permissions, viewsets, authentication
from rest_framework.authentication import TokenAuthentication
from .models import Payment
from .serializers import PaymentSerializer

class PaymentViewSet(viewsets.ModelViewSet):
    """CRUD Payment"""
    authentication_classes = (TokenAuthentication,)
    serializer_class = PaymentSerializer
    queryset = Payment.objects.all()
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes =(permissions.IsAuthenticated,)
