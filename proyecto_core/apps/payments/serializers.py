from rest_framework import serializers
from .models import Payment

class PaymentSerializer(serializers.ModelSerializer):
    """Payment serializar"""
    class Meta:
        model = Payment
        fields = ('id','date_payment', 'status','payment_method','orders', 'value',)
