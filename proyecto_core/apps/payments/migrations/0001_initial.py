# Generated by Django 3.2.5 on 2021-07-23 04:22

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_payment', models.DateTimeField()),
                ('status', models.CharField(choices=[('PE', 'pending'), ('CO', 'completed'), ('RE', 'rejected')], max_length=255)),
                ('value', models.DecimalField(decimal_places=2, max_digits=12)),
                ('payment_method', models.CharField(choices=[('CA', 'cash'), ('DC', 'debit card'), ('CC', 'credit card')], max_length=255)),
            ],
            options={
                'verbose_name': 'Payment',
                'verbose_name_plural': 'Payments',
            },
        ),
    ]
