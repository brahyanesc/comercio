from django.db import models

class Payment(models.Model):
    """Payment model"""
    class Status(models.TextChoices):
        PENDING = 'PE', 'pending'
        COMPLETED = 'CO', 'completed'
        REJECTED = 'RE', 'rejected'

    class PaymentMethods(models.TextChoices):
        CASH = 'CA', 'cash'
        DEBIT_CARD = 'DC', 'debit card'
        CREDIT_CARD = 'CC', 'credit card'
 
    date_payment = models.DateTimeField()
    status = models.CharField(max_length=255, choices=Status.choices)
    value = models.DecimalField(decimal_places=2, max_digits=12)
    payment_method = models.CharField(max_length=255, choices=PaymentMethods.choices)

    class Meta:
        verbose_name='Payment'
        verbose_name_plural='Payments'
    
    def __str__(self):
        return 'payment #: {} date: {} is {}'.format(self.pk, self.date_payment, self.status) 