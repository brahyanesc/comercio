from django.test import TestCase
from django.contrib.auth import get_user_model

class UserModelTest(TestCase):
    """Test case for User"""
    def test_create_user_with_email_successful(self):
        """try create a user with email"""
        email = 'test@prueba.com'
        name = 'nameTest'
        password = 'TestPassword123'
        user = get_user_model().objects.create_user(
            email=email,
            name=name,
            password=password
        )

        self.assertEqual(user.email, email)
        self.assertEqual(user.name, name)
        self.assertTrue(user.check_password(password))

    def test_new_user_email_normalized(self):
        """Test normalize email user"""
        email = 'test@PRUEba.com'
        user = get_user_model().objects.create_user(
            email=email,
            name='nameTest',
            password='TestPassword123'
        )
        self.assertEqual(user.email, email.lower())

    def test_new_user_invalid_email(self):
        """Test new user with invalid email"""
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(
                email=None,
                name='nameTest',
                password='TestPassword123'
        )

    def test_create_new_super_user_successful(self):
        email = 'test@prueba.com'
        name = 'nameTest'
        password = 'TestPassword123'
        user = get_user_model().objects.create_superuser(
            email=email,
            name=name,
            password=password
        )

        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)