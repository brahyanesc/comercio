from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

from rest_framework.test import APIClient
from rest_framework import status


def create_user(**params):
    return get_user_model().objects.create_user(**params)

class PublicUserApiTest(TestCase):
    """Test public user api"""
    def setUp(self):
        self.user = create_user(
            email = 'token@prueba.com',
            name = 'testName',
            password = 'testPassword',
        )

        self.client = APIClient()
        self.USER_LIST = reverse('user-list')
        self.USER_DETAIL = reverse('user-detail', kwargs={'pk': self.user.pk})
        self.TOKEN_URL = reverse('token')

    def test_create_token(self):
        """Test create token for user"""

        payload = {
            'email':'tokencreate@prueba.com',
            'name': 'testName',
            'password': 'testPassword'
        }
        create_user(**payload)
                
        URL = self.TOKEN_URL
        res = self.client.post(URL,payload)

        self.assertIn('token',res.data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_create_token_with_invalid_credentials(self):
        """Test create token with invalid credentials"""

        payload_wrong = {
            'email': self.user.email,
            'name': self.user.name,
            'password': 'badPass'
        }
        
        URL = self.TOKEN_URL
        res = self.client.post(URL,payload_wrong)
        self.assertNotIn('token',res.data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_token_when_user_no_exist(self):
        """Test creat token when user no exist"""
        payload = {
            'email': 'test3@prueba.com',
            'name': self.user.name,
            'password': self.user.password
        }
        URL = self.TOKEN_URL
        res = self.client.post(URL, payload)
        self.assertNotIn('token', res.data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_retrieve_user_unauthorized(self):
        """Test the authentication is required for user"""
        res = self.client.get(self.USER_DETAIL)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)

class PrivateUserApiTest(TestCase):
    """Test private user api"""

    def setUp(self):
        self.user = create_user(
            email = 'test@prueba.com',
            name = 'testName',
            password = 'testPassword',
        )

        self.USER_LIST = reverse('user-list')
        self.USER_DETAIL = 'user-detail'
        self.CREATE_USER_URL = reverse('user-list')
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)

    def test_retrieve_user_success(self):
        """Test get method for detail user"""
        res_data = {
            'email': self.user.email,
            'name': self.user.name
        } 
        URL = reverse(self.USER_DETAIL, kwargs={'pk': self.user.pk})
        res = self.client.get(URL)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data['email'], res_data['email'])


    def test_post_user_success(self):
        """ Test for create new user success"""

        payload = {
            'email': 'nuevo@correo.com',
            'name': 'newName',
            'password': 'newPasss'
        } 

        res = self.client.post(self.USER_LIST,payload)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        user = get_user_model().objects.get(**res.data)
        self.assertTrue(user.check_password(payload['password']))
        self.assertNotIn('password', res.data)

    def test_put_user_success(self):
        """ Test for update user success"""

        user = create_user(
            email = 'update@prueba.com',
            name = 'testUpdateName',
            password = 'testPassword',
        )

        payload = {
            'email': user.email,
            'name': 'newName',
            'password': user.password
        } 
        URL = reverse(self.USER_DETAIL, kwargs={'pk': user.pk})
        
        res = self.client.put(URL,payload)
        user.refresh_from_db()
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data['name'], payload['name'])
        
    def test_delete_user_success(self):
        """ Test for delete user success"""

        user = create_user(
            email = 'delete@prueba.com',
            name = 'testDeleteName',
            password = 'testPassword',
        )


        URL = reverse(self.USER_DETAIL, kwargs={'pk': user.pk})
        
        res = self.client.delete(URL)
        self.assertEqual(res.status_code, status.HTTP_204_NO_CONTENT)
        user_exists = get_user_model().objects.filter(pk=user.pk)
        self.assertFalse(user_exists)

    def test_user_exist(self):
        """Test create user already exist"""
        payload = {
            'email': 'test@prueba.com',
            'name': 'testName',
            'password': 'passwordTest'
        }
        res = self.client.post(self.CREATE_USER_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
