from rest_framework import permissions, serializers, viewsets, authentication
from .serializers import AuthTokenSerializer, UserSerializer
from .models import User
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings
from .notifications import sendMailNewUser

class UserViewSet(viewsets.ModelViewSet):
    """CRUD for User"""
    serializer_class = UserSerializer
    queryset = User.objects.all()
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes =(permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save()
        sendMailNewUser(self.request.data['email'])
        



class CreateTokenApiView(ObtainAuthToken):
    """Create authentication token"""
    serializer_class = AuthTokenSerializer
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES
