from rest_framework import serializers
from django.contrib.auth import get_user_model, authenticate



class UserSerializer(serializers.ModelSerializer):
    """User serializer"""

    class Meta:
        model = get_user_model()
        fields = ('id','email','name','password')
        extra_kwargs = {
            'password': {
                'write_only': True,
                'style': {'input_type':'password'}
            }
        }

    def create(sefl,validated_data):
        """Create and return a new user"""
        user = get_user_model().objects.create_user(
            email=validated_data['email'],
            name=validated_data['name'],
            password=validated_data['password']
        )
        return user

    def update(self, instance, validated_data):
        """Update user"""
        if 'password' in validated_data:
            password = validated_data.pop('password')
            instance.set_password(password)
        return super().update(instance, validated_data)

class AuthTokenSerializer(serializers.Serializer):
    """Serializer for auth user"""
    email = serializers.CharField()
    password = serializers.CharField(
        style={'input_type':'password'}
    )

    def validate(self, attrs):
        """Validate and auth user"""
        email = attrs.get('email')
        password = attrs.get('password')

        user = authenticate(
            request=self.context.get('request'),
            username= email,
            password=password
        )

        if not user:
            mesessage = 'Could not be authenticated with the credentials entered'
            raise serializers.ValidationError(mesessage, code='authorization')

        attrs['user'] = user
        return attrs

