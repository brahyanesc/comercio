from . import views
from django.urls import path

urlpatterns = [
    path('token/', views.CreateTokenApiView.as_view(), name='token'),
]

