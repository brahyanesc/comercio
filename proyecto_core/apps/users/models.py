from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import (
        AbstractBaseUser, BaseUserManager, PermissionsMixin)
from django.utils import timezone
from django.utils.translation import ugettext_lazy

class UserManager(BaseUserManager):
    """User Manager"""

    def create_user(self, email, name, password, **extra_fields):
        """Create user"""
        now = timezone.now()
        if not email:
            raise ValueError('users must have an email address')
        email = self.normalize_email(email)
        user = self.model(email = email,
                            name = name,
                            date_joined = now,
                            **extra_fields)
        user.set_password(password)
        user.save(using = self._db)
        return user

    def create_superuser(self, email,name, password, **extra_fields):
        """Create super user"""
        user = self.create_user(email,name, password, **extra_fields)
        user.is_superuser = True
        user.is_staff = True
        user.save(using = self._db)
        return user

class User(AbstractBaseUser,PermissionsMixin):
    """custom user class"""

    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=100)
    date_joined = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    class Meta:
        verbose_name = ugettext_lazy('usuario')
        verbose_name_plural = ugettext_lazy('usuarios')

    def get_full_name(self):
        """Get full name"""
        return self.name

    def get_short_name(self):
        """Get short name"""
        return self.name

    def __str__(self):
        """Return string user"""
        return self.email