from rest_framework import serializers
from .models import Delivery

class DeliverySerializer(serializers.ModelSerializer):
    """Delivery serializar"""
    class Meta:
        model = Delivery
        fields = ('id','name', 'delivery_date','status','shipping_method', 'addres','orders')


