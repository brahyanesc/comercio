from rest_framework import permissions, viewsets, authentication
from rest_framework.authentication import TokenAuthentication
from .models import Delivery, Status
from .serializers import DeliverySerializer
from .notifications import sendMailStatusDelivery

class DeliveryViewSet(viewsets.ModelViewSet):
    """CRUD Delivery"""
    authentication_classes = (TokenAuthentication,)
    serializer_class = DeliverySerializer
    queryset = Delivery.objects.all()
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes =(permissions.IsAuthenticated,)

    def perform_udate(self, serializer):
        serializer.save()
        
        if self.request.data['status'] == Status.SENT :
            sendMailStatusDelivery(self.request.data['email'],Status.SENT.label)

        if self.request.data['status'] == Status.DELIVERED :
            sendMailStatusDelivery(self.request.data['email'],Status.DELIVERED.label)