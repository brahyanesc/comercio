# Generated by Django 3.2.5 on 2021-07-23 04:22

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Delivery',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('delivery_date', models.DateTimeField()),
                ('status', models.CharField(choices=[('pr', 'processing'), ('pa', 'packed'), ('se', 'sent'), ('de', 'delivered')], max_length=255)),
                ('shipping_method', models.CharField(choices=[('sp', 'store pickup'), ('sh', 'send home')], max_length=255)),
                ('addres', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name': 'Delivery',
                'verbose_name_plural': 'Deliveries',
            },
        ),
    ]
