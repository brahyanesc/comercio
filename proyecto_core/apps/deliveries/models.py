from django.db import models

class Status(models.TextChoices):
    PROCESSING = 'pr', 'processing'
    PACKED = 'pa', 'packed'
    SENT = 'se', 'sent'
    DELIVERED = 'de', 'delivered'

class ShippingMethod(models.TextChoices):
    STORE_PICKUP = 'sp', 'store pickup'
    SEND_HOME = 'sh', 'send home'  

class Delivery(models.Model):
    """delivery model"""

    name = models.CharField(max_length=255)
    delivery_date = models.DateTimeField()
    status = models.CharField(max_length=255, choices=Status.choices)
    shipping_method = models.CharField(max_length=255, choices=ShippingMethod.choices)
    addres = models.CharField(max_length=255)
    
    class Meta:
        verbose_name='Delivery'
        verbose_name_plural='Deliveries'
    
    def __str__(self):
        return self.delivery_date + self.name