## Omnilatam coding chanllenge


### Install prerequisites

* execute: sudo apt-get update
* execute: sudo apt-get install rabbitmq-server

##### Running the development environment

* python version 3.8.10
* on comercio folder 
* from your python version create de virtual enviroment
* execute: python3.8 -m venv env
* active the enviroment
* execute: source env/bin/activate
* execute: pip install -r requirements/local.txt

##### Create relational database sqlite and migrations

* execute: python manage.py makemigrations
* execute: python manage.py migrate
* execute: python manage.py createsuperuser

##### Run celery and django app
 
 * open 2 terminal on proyecto core 
 * In the first terminal execute: celery -A proyecto_core worker -l info 
 * in the second terminal execute: python manage.py runserver 

 #### open the api

 * open in a browser http://127.0.0.1:8000/api/ for view the api root
 * for use the api you need generate a token
 * you can generate a token for super user created previously on http://127.0.0.1:8000/api/token/
 * if you want to view the api on browser you can install a chrome extension mod header
 * you can add Authorization token ############

 #### Run coverage

* execute: coverage run --source='.' manage.py test
* execute: coverage html or coverage report


